/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.siskopat.muamalat.dao;

import com.siskopat.muamalat.model.Obat2;
import com.siskopat.muamalat.service.ObatService;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author 20140307
 */
@Service
public class ObatServiceImpl implements ObatService {

    private EntityManagerFactory emf;

    @Autowired
    public void setEmf(EntityManagerFactory emf) {
        this.emf = emf;
    }

    @Override
    public List<Obat2> ListObat() {
        EntityManager em = emf.createEntityManager();
        return em.createQuery("from Obat2", Obat2.class).getResultList();
    }

    @Override
    public Obat2 saveOrUpdate(Obat2 obat2) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        Obat2 saved = em.merge(obat2);
        em.getTransaction().commit();

        return saved;
    }

    @Override
    public Obat2 getIdObat(int id) {
        EntityManager em = emf.createEntityManager();
        return em.find(Obat2.class, id);
    }

    @Override
    public void hapus(int id) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.remove(em.find(Obat2.class, id));
        em.getTransaction().commit();
    }

}
