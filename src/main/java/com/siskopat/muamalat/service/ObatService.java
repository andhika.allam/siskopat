/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.siskopat.muamalat.service;

import com.siskopat.muamalat.model.Obat2;
import java.util.List;

/**
 *
 * @author 20140307
 */
public interface ObatService {

    List<Obat2> ListObat();

    Obat2 saveOrUpdate(Obat2 obat2);

    Obat2 getIdObat(int id);

    void hapus(int id);

}
