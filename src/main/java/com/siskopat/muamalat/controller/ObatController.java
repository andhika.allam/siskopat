/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.siskopat.muamalat.controller;

import com.siskopat.muamalat.model.Obat2;
import com.siskopat.muamalat.service.ObatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author 20140307
 */

@Controller
public class ObatController {
    
    private ObatService obatService;
    
    @Autowired
    public void setObat(ObatService oS){
        this.obatService = oS;
    }
    
    @RequestMapping("/obat")
    public String obatList(Model model){
        model.addAttribute("obat2", obatService.ListObat());
        
        return "obat";
    }
 
    @RequestMapping(value = "obat/create", method = RequestMethod.POST)
    public String tampilkanData(Model model) {
        model.addAttribute("obat2", new Obat2());
        return "formObat";
    }

    @RequestMapping(value = "obat/create", method = RequestMethod.POST)
    public String simpanData(Model model, Obat2 oBat2) {
        model.addAttribute("obat", obatService.ListObat());
        return "redirect:/obat";
    }
    

    @RequestMapping(value = "obat/edit/{id}", method = RequestMethod.GET)
    public String editData(@PathVariable Integer id, Model model) {
        model.addAttribute("obat", obatService.getIdObat(id));
        return "formObat";
    }

    @RequestMapping(value = "user/create/{id}")
    public String hapusData(@PathVariable Integer id) {
        obatService.hapus(id);
        return "redirect:/obat";
    }   

    
    
}
