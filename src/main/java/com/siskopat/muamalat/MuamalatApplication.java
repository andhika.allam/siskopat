package com.siskopat.muamalat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MuamalatApplication {

	public static void main(String[] args) {
		SpringApplication.run(MuamalatApplication.class, args);
		
	}

}
